# Administrador de gastos

Este proyecto tiene como objetivo que el usuario pueda administrar y realizar un seguimiento detallado de sus gastos a partir de un presupuesto inicial. Esta herramienta permite que la persona que lo utilice pueda tener un mejor manejo de sus finanzas. 

El mismo es el proyecto 3 de 10 del curso de Vue.js 3 tomado en Udemy. El propósito de estos es formarme con las bases fundamentales de esta tecnologia aplicando de manera práctica a proyectos realistas.

- Se encuentra levantado en https://administrador-de-gastos-sandy.vercel.app/ 
